# BPM #

Basic (web/app) Project Management for Emacs.

### What is it? ###

* Just a link lister, really.
* [Read about it in french](http://linuxfr.org/users/philippemc/journaux/peigner-la-girafe-bpm)

### How do I set it up? ###

Put it in your *load-path* and require it. Or just eval' all the functions for quick testing.

### How do I run it? ###


```
#!emacs-lisp

M-x bpm
```
Enter a single file name, or a directory (it will ask you for an optional filename regexp filter, just enter "*.py" or "php").