;;; bpm.el --- Basic (web/app) Project Management: List file dependancies

;; URL: https://bitbucket.org/xaccrocheur/bpm
;; Maintainer: xaccrocheur@gmail.com
;; Keywords: ide, project

;; This file is NOT part of GNU Emacs.

;; BPM is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; BPM is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

(require 'org)

(defconst bpm-blacklist (list
                         "getid3.php"
                         "libs/getid3/getid3.php"
                       )
  "The files NOT to read")

(defvar bpm-buffer "BPM"
  "The BPM buffer")

(defun bpm-test-url (url bpm-fpath)
  "Try to open URL and return (propertized) ERR_MSG. Called in `bpm'."
  (let ((url-full (concat bpm-fpath url)))
    (if (not (file-exists-p (expand-file-name url-full)))
        (if (or (string-prefix-p "http" url)
                (string-prefix-p "//" url))
            (propertize "External" 'font-lock-face 'font-lock-keyword-face)
          (propertize "Error" 'font-lock-face 'font-lock-warning-face))
      "")))

;; (setq bpm-test-list '())

;; (setq bpm-test-list-family
;;  '((:father . "plop")
;;    (:child . "plop")
;;    (:grand-child . "plop")))

(defun bpm-read-file (fname)
  "Extract elements from FNAME. Mapce'd from `bpm'."

  (defun bpm-parse (fname bpm-regexp bpm-elm-list)
    "Read FNAME in a temp buffer and run BPM-REGEXP upon it and its matches, then add the org-formatted result to BPM-ELM-LIST.
Internal defun of `bpm-read-file'."
    (if (not (string-prefix-p ".#" (file-name-nondirectory fname)))
        (with-temp-buffer
          (insert-file-contents-literally fname)
          (goto-char (point-min))
          (setq bpm-base-name (file-name-nondirectory fname))
          (setq bpm-fpath (file-name-directory fname))
          (setq bpm-freal-name (concat bpm-fpath bpm-base-name))
          ;; (message "Reading %s" fname)
          (while (re-search-forward bpm-regexp nil t)
            (when (match-string 0)
              (let ((url (match-string 1))
                    (title (match-string 2))
                    ;; (line-one (what-line))
                    )
                (progn
                  ;; (add-to-list 'bpm-test-list url)
                  ;; (push (cons fname (cons (concat bpm-fpath url) line-one)) bpm-test-list)
                  (add-to-list bpm-elm-list
                               (concat "- " (org-make-link-string (concat bpm-fpath url) url) " " (bpm-test-url url bpm-fpath) "\n"))
                  (if (not (string= url "libs/getid3/getid3.php"))
                      (progn

                        ;; (add-to-list 'bpm-test-list-family`(:father ,fname))
                        ;; (add-to-list 'bpm-test-list-family `(:child ,url))

                        (if (and (not (file-accessible-directory-p (concat bpm-fpath url)))
                                 (file-readable-p (concat bpm-fpath url)))
                            (with-temp-buffer
                              (message "Reading %s" (concat bpm-fpath url))
                              (insert-file-contents-literally (concat bpm-fpath url))
                              (goto-char (point-min))
                              (while (re-search-forward bpm-regexp nil t)
                                (when (match-string 0)
                                  (let ((url-deep (match-string 1))
                                        (title-deep (match-string 2))
                                        ;; (line-two (what-line))
                                        )
                                    (progn
                                      ;; (push (list fname (list (concat bpm-fpath url) line-one) (list (concat bpm-fpath url-deep) line-two))
                                      ;; bpm-test-list)
                                      ;; (add-to-list 'bpm-test-list
                                                   ;; (list fname (list (concat bpm-fpath url) line-one) (list (concat bpm-fpath url-deep) line-two)))
                                      (if bpm-iter-p
                                          (add-to-list bpm-elm-list "\n**** Also\n" t))
                                      (add-to-list bpm-elm-list
                                                   (concat
                                                    "- " (org-make-link-string (concat bpm-fpath url-deep) url-deep)
                                                    " (in " (org-make-link-string (concat bpm-fpath url) url) ") "
                                                    (bpm-test-url url-deep bpm-fpath) "\n") t)
                                      (setq bpm-iter-p nil))))))))))))))))

  (let
      ((list-href '())
       (list-script '())
       (list-css '())
       (list-require '())
       (list-ajax-call '())
       (bpm-iter-p t))
    (progn

      (bpm-parse fname (rx
                        "<a "
                        (*? anything)
                        word-start "href="
                        (or "'" "\"")
                        (group (* (not (any "'" "\""))))
                        (or "'" "\"")
                        (*? anything) ">"
                        (group (*? anything))
                        "</a>")
                 'list-href)

      (bpm-parse fname (rx
                        "<script "
                        (*? anything)
                        word-start "src="
                        (or "'" "\"")
                        (group (* (not (any "'" "\""))))
                        (or "'" "\"")
                        (*? anything) ">")
                 'list-script)

      (bpm-parse fname (rx
                        "<link "
                        (* (not (any "rel=\"icon\"")))
                        word-start "href="
                        (or "'" "\"")
                        (group (* (not (any "'" "\""))))
                        (or "'" "\"")
                        (*? anything) ">")
                 'list-css)

      (bpm-parse fname (rx
                        bol
                        "require"
                        (*? anything)
                        (or "'" "\"")
                        (group (* (not (any "'" "\""))))
                        (or "'" "\"")
                        (*? anything))
                 'list-require)

      (bpm-parse fname (rx
                        bol
                        (*? anything)
                        (or "'" "\"")
                        (group
                         (* (not (any "'" "\"")))
                         ".php"
                         )
                        (or "'" "\"")
                        (*? anything)
                        )
                 'list-ajax-call)

      (with-current-buffer bpm-buffer
        (insert (concat "** " (propertize "File " 'font-lock-face (if (or (car list-href)
                                                                          (car list-script)
                                                                          (car list-css)
                                                                          (car list-require)
                                                                          (car list-ajax-call))
                                                                      'font-lock-variable-name-face
                                                                    'font-lock-builtin-face)) (org-make-link-string fname fname) " \n"))
        (if (car list-href) (progn (insert "\n*** HREF Links\n")
                                   (mapc 'insert list-href)))
        (if (car list-script) (progn (insert "\n*** SCRIPT Links\n")
                                     (mapc 'insert list-script)))
        (if (car list-css) (progn (insert "\n*** CSS Links\n")
                                  (mapc 'insert list-css)))
        (if (car list-require) (progn (insert "\n*** Requires\n")
                                      (mapc 'insert list-require)))
        (if (car list-ajax-call) (progn (insert "\n*** Other (Possibly Ajax) calls\n")
                                        (mapc 'insert list-ajax-call)))
        (insert "\n" (concat (make-string (- (frame-width) 3) ?-) "\n"))))))

(defun bpm (bpm-prj-root &optional bpm-ext-filter)
  "List all links in BPM-PRJ-ROOT using `bpm-read-file'.
Basic (web/app) Project Management."
  (interactive
   (let* ((bpm-prj-root (read-file-name "Project root directory (or list of files) "))
          (bpm-ext-filter (if (file-accessible-directory-p bpm-prj-root)
                              (read-regexp "Filter on extension (regexp)? "))))
     (list bpm-prj-root bpm-ext-filter)))

  (if (get-buffer bpm-buffer) (kill-buffer bpm-buffer))

  (with-current-buffer (get-buffer-create bpm-buffer)
    (insert (concat "* File dependencies for " (org-make-link-string bpm-prj-root bpm-prj-root) " (" (format-time-string "%Y-%m-%d %T") ")\n\n")))

  (if bpm-ext-filter
      (mapc 'bpm-read-file (directory-files bpm-prj-root t (concat "\\." bpm-ext-filter "$")))
    (mapc 'bpm-read-file `(,bpm-prj-root)))

  (switch-to-buffer bpm-buffer)
  (org-mode)
  (goto-char (point-min))
  (org-cycle))

(provide 'bpm)
